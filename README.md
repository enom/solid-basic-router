# solid-basic-router

A very basic hash change router for [Solid](https://www.solidjs.com/).

[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)
[![Pipeline Status](https://gitlab.com/enom/solid-basic-router/badges/master/pipeline.svg)](https://gitlab.com/enom/solid-basic-router/-/commits/master)
[![Coverage Status](https://coveralls.io/repos/gitlab/enom/solid-basic-router/badge.svg?branch=master)](https://coveralls.io/gitlab/enom/solid-basic-router?branch=master)

This package uses `window.location.hash` and the `hashchange` event to handle routing.

Documentation is available [here](https://enom.gitlab.io/solid-basic-router/).

## Getting Started

Add `solid-basic-router` to your `package.json`.

```sh
npm install --save solid-js solid-basic-router
```

### Example

```js
//
// @todo insert playground link
//
import { render } from 'solid-js/web'
import { Link, Route, Router, Routes, useRouter } from 'solid-basic-router'

/**
 * Example application using solid-basic-router.
 *
 * @returns {JSX} Demo app
 */
function App () {
  const [router, { match, to }] = useRouter()

  const other = () => to(
    router.route === '/projects/alpha'
      ? '/projects/bravo'
      : '/projects/alpha'
  )

  return (
    <>
      <nav>
        <Link to='/'>Home</Link>
        <Link to='/about'>About</Link>
        <Link to='/projects' exact>Projects</Link>
        <Link to='/projects/alpha'>Project A</Link>
        <Link to='/projects/bravo'>Project B</Link>
      </nav>

      <Routes fallback={<h2>404 Page</h2>}>
        <Route is='/'>
          <h2>Home Page</h2>
        </Route>

        <Route is='/about' view={<h2>About Page</h2>} />

        <Route match='/projects'>
          <h2>Projects Page</h2>
          <p>Slug: {match('/projects/:slug')?.slug ?? 'undefined'}</p>
          <p><button onClick={other}>Other Project</button></p>

          <Routes>
            <Route is='/projects/alpha'>
              <h3>Alpha Project</h3>
            </Route>

            <Route is='/projects/bravo'>
              <h3>Bravo Project</h3>
            </Route>
          </Routes>
        </Route>
      </Routes>
    </>
  )
}

render(() => (
  <Router>
    <App />
  </Router>
), document.getElementById('root'))
```
