var $4WlUo$solidjsweb = require("solid-js/web");
var $4WlUo$solidjs = require("solid-js");
var $4WlUo$solidjsstore = require("solid-js/store");

"use strict";
Object.defineProperty(module.exports, "__esModule", {
    value: true
});
module.exports.getParams = $a1adfa6e87ad916f$var$getParams;
module.exports.getRoute = $a1adfa6e87ad916f$var$getRoute;
module.exports.Link = $a1adfa6e87ad916f$var$Link;
module.exports.Route = $a1adfa6e87ad916f$var$Route;
module.exports.Router = $a1adfa6e87ad916f$var$Router;
module.exports.Routes = $a1adfa6e87ad916f$var$Routes;
module.exports.setRoute = $a1adfa6e87ad916f$var$setRoute;
module.exports.useRouter = $a1adfa6e87ad916f$var$useRouter;
module.exports.RouterSettings = void 0;



const $a1adfa6e87ad916f$var$_tmpl$ = $4WlUo$solidjsweb.template(`<a></a>`, 2);
const $a1adfa6e87ad916f$var$RouterContext = $4WlUo$solidjs.createContext();
/**
 * Global router settings.
 *
 * These are not meant to be reactive.
 *
 * @namespace RouterSettings
 * @property {string} [activeCssClass] CSS class to add to active all Link components
 * @property {boolean} [exactActiveLinks] Use exact route matching for all Link components
 * @see RouterSettings#configure
 * @example
 * import { RouterSettings } from 'solid-basic-router'
 * @example
 * // Manually configuring the router
 * RouterSettings.activeCssClass = 'ui.active'
 */ const $a1adfa6e87ad916f$var$RouterSettings = {
    activeCssClass: 'active',
    exactActiveLinks: false,
    /**
   * Configures the global router settings.
   *
   * @function RouterSettings#configure
   * @param {object} settings Settings to configure
   * @returns {void}
   * @see RouterSettings
   * @example
   * import { RouterSettings } from 'solid-basic-router'
   * @example
   * // Using the `configure` helper method
   * RouterSettings.configure({
   *   activeCssClass: 'ui.active',
   *   exactActiveLinks: true
   * })
   */ configure (settings) {
        Object.assign($a1adfa6e87ad916f$var$RouterSettings, settings);
    }
};
/**
 * Returns the matched parameters of a given route as an object.
 *
 * Parameters are specified using ":" followed by the capture name desired(e.g. ":slug").
 *
 * @param {string} match Parameterized route
 * @param {string} route Route to match
 * @returns {object|undefined} Matched route parameters or undefined
 * @example
 * import { getParams } from 'solid-basic-router'
 * @example
 * // Even though no parameters are being captured, since the route matches, it returns an object
 * console.log(getParams('/projects', '/projects/alpha/authors/charlie'))
 * // object: {}
 * @example
 * // Specifying parameters
 * console.log(getParams('/projects/:slug', '/projects/alpha/authors/charlie'))
 * // object: { slug: 'alpha' }
 * @example
 * // Specifying multiple parameters
 * console.log(getParams('/projects/:slug/authors/:username', '/projects/alpha/authors/charlie'))
 * // object: { slug: 'alpha', username: 'charlie' }
 * @example
 * // Route doesn't match so undefined is returned
 * console.log(getParams('/projects/:slug/authors/:username', '/authors/charlie'))
 * // undefined
 */ module.exports.RouterSettings = $a1adfa6e87ad916f$var$RouterSettings;
function $a1adfa6e87ad916f$var$getParams(match, route) {
    if (match === '/') return {
    };
    const params = {
    };
    const matched = match.slice(1).split('/');
    const routed = route.slice(1).split('/');
    const length = routed.length > matched.length ? routed.length : matched.length;
    for(let i = 0; i < length; i++){
        if (routed[i] === undefined) return;
        if (matched[i] === undefined) return params;
        if (matched[i][0] === ':') params[matched[i].slice(1)] = routed[i];
        else if (matched[i] !== routed[i]) return;
    }
    return params;
}
/**
 * Returns the current route based on the location hash.
 *
 * @returns {string} Current route
 * @example
 * // Location: http://localhost/
 * console.log(getRoute())
 * // string: '/'
 * @example
 * // Location: http://localhost/#/about
 * console.log(getRoute())
 * // string: '/about'
 */ function $a1adfa6e87ad916f$var$getRoute() {
    const hash = window.location.hash;
    return hash ? hash.slice(1) : '/';
}
/**
 * Creates an anchor tag to the given route.
 *
 * The contents of the link can also be specified using `props.label`.
 *
 * By default, the link will inherit the {@link RouterSettings} `activeCssClass`
 * class when the current route matches the given one. The added class can be
 * overriden per component by passing one to `props.active` or disabled by passing
 * `false`.
 *
 * The matching behaviour can be adjusted using `props.exact` or by setting the
 * {@link RouterSettings} `exactActiveLinks` to `true`. The global setting can also
 * be disabled per component by passing `false` to `props.exact`.
 *
 * Using exact matching leverages {@link RouterActions#is} while fuzzy matching
 * uses {@link RouterActions#match}. Both of these require the application code to
 * be wrapped with a {@link Router}.
 *
 * @param {object} props Component properties
 * @param {string} props.to Route to link to
 * @param {string} [props.active] Active CSS class to add
 * @param {string|JSX} [props.label] Label to display instead of component children
 * @returns {JSX} Configured anchor tag
 * @see RouterSettings
 * @see RouterActions#is
 * @see RouterActions#match
 * @example
 * import { Link } from 'solid-basic-router'
 * @example
 * <!-- JSX -->
 * <Link to='/'>Home</Link>
 * @example
 * <!-- Using the `label` property -->
 * <Link to='/' label='Home' />
 * @example
 * <!-- Using the `active` property -->
 * <Link to='/' active='ui.active'>Home</Link>
 * @example
 * <!-- Disabling the global `activeCssClass` setting -->
 * <Link to='/' active={false}>Home</Link>
 * @example
 * <!-- Using the `exact` property -->
 * <Link to='/projects' exact>Projects</Link>
 * @example
 * <!-- Disabling the global `exactActiveLinks` setting -->
 * <Link to='/projects' exact={false}>Projects</Link>
 */ function $a1adfa6e87ad916f$var$Link(props) {
    const [, { is: is , match: match  }] = $a1adfa6e87ad916f$var$useRouter();
    const resolve = (to)=>{
        if (props.exact === true) return is(to);
        if ($a1adfa6e87ad916f$var$RouterSettings.exactActiveLinks && props.exact !== false) return is(to);
        return to === '/' ? is(to) : match(to);
    };
    const list = ()=>{
        var _classList;
        const results = (_classList = props.classList) !== null && _classList !== void 0 ? _classList : {
        };
        if (props.active) return Object.assign(results, {
            [props.active]: resolve(props.to)
        });
        if ($a1adfa6e87ad916f$var$RouterSettings.activeCssClass && props.active !== false) return Object.assign(results, {
            [$a1adfa6e87ad916f$var$RouterSettings.activeCssClass]: resolve(props.to)
        });
        return results;
    };
    return (()=>{
        const _el$ = $a1adfa6e87ad916f$var$_tmpl$.cloneNode(true);
        var _label;
        $4WlUo$solidjsweb.insert(_el$, ()=>(_label = props.label) !== null && _label !== void 0 ? _label : props.children
        );
        $4WlUo$solidjsweb.effect((_p$)=>{
            const _v$ = '#' + props.to, _v$2 = list();
            _v$ !== _p$._v$ && $4WlUo$solidjsweb.setAttribute(_el$, "href", _p$._v$ = _v$);
            _p$._v$2 = $4WlUo$solidjsweb.classList(_el$, _v$2, _p$._v$2);
            return _p$;
        }, {
            _v$: undefined,
            _v$2: undefined
        });
        return _el$;
    })();
}
/**
 * Handles matching a given route and displaying its contents.
 *
 * This is simply a configured `Match` component exported by Solid. One of `props.is`
 * or `props.match` is required.
 *
 * The contents to render can also be specified using `props.view`.
 *
 * Parent must be a {@link Routes} or `Switch` component.
 *
 * @function Route
 * @param {object} props Component properties
 * @param {string} [props.is] Exact route to match
 * @param {string} [props.match] Route to match
 * @param {JSX} [props.view] View to render
 * @returns {JSX} Configured Match component
 * @see RouterActions#is
 * @see RouterActions#match
 * @see https://www.solidjs.com/docs/latest/api#%3Cswitch%3E%2F%3Cmatch%3E
 * @example
 * import { Route } from 'solid-basic-router'
 * @example
 * <!-- JSX -->
 * <Route is='/'><h2>Home Page</h2></Route>
 * @example
 * <!-- Using the `view` property -->
 * <Route is='/' view={<h2>Home Page</h2>} />
 * @example
 * <!-- Using the `match` property -->
 * <Route match='/projects'><h2>Projects <small>and Sub Projects</small> Pages</h2></Route>
 * @example
 * <!-- Manually creating a route -->
 * <Match when={is('/')}><h2>Home Page</h2></Match>
 */ function $a1adfa6e87ad916f$var$Route(props) {
    const [, { is: is , match: match  }] = $a1adfa6e87ad916f$var$useRouter();
    return (()=>{
        const _c$ = $4WlUo$solidjsweb.memo(()=>!!props.match
        , true);
        return $4WlUo$solidjsweb.createComponent($4WlUo$solidjsweb.Match, {
            get when () {
                return _c$() ? match(props.match) : is(props.is);
            },
            get children () {
                var _view;
                return (_view = props.view) !== null && _view !== void 0 ? _view : props.children;
            }
        });
    })();
}
/**
 * Router context provider.
 *
 * @param {object} props Router properties
 * @param {JSX} props.children JSX to make router aware
 * @returns {JSX} Router aware JSX
 * @example
 * import { render } from 'solid-js/web'
 * import { Router } from 'solid-basic-router'
 * import App from './App'
 *
 * render(() => (
 *   <Router>
 *     <App />
 *   </Router>
 * ), document.getElementById('root'))
 */ function $a1adfa6e87ad916f$var$Router(props) {
    const [state, setState] = $4WlUo$solidjsstore.createStore({
        route: $a1adfa6e87ad916f$var$getRoute()
    });
    const actions = {
        /**
     * Changes the current route to another.
     *
     * @function RouterActions#to
     * @param {string} route Route to navigate to
     * @param {boolean} [replace=false] Use location.replace instead of hashchange event
     * @returns {void}
     * @see setRoute
     * @example
     * import { useRouter } from 'solid-basic-router'
     * @example
     * function App () {
     *   const [, { to }] = useRouter()
     *
     *   return (
     *     <button onClick={() => to('/')}>To Home Page</button>
     *   )
     * }
     */ to: $a1adfa6e87ad916f$var$setRoute,
        /**
     * Checks if the current route matches the given one exactly.
     *
     * Trailing slashes matter:
     *
     * - `/about` does not match `/about/` and vice versa
     *
     * @function RouterActions#is
     * @param {string} route Route to check
     * @returns {boolean} Route matches exactly
     * @example
     * import { useRouter } from 'solid-basic-router'
     * @example
     * function App () {
     *   const [, { is }] = useRouter()
     *
     *   return (
     *     <p>Are we currently on the home page? {is('/') ? 'Yes' : 'No'}</p>
     *   )
     * }
     */ is (route) {
            return route === state.route;
        },
        /**
     * Matches the current route to a given one and returns the captured parameters.
     *
     * Trailing slashes matter:
     *
     * - `/projects` matches `/projects`, `/projects/alpha`, and `projects/bravo`
     * - `/projects/` matches `projects/alpha` and `projects/bravo` but not `/projects`
     *
     * @function RouterActions#match
     * @param {string} route Route to check
     * @returns {object|undefined} Matched route parameters or undefined
     * @see getParams
     * @example
     * import { useRouter } from 'solid-basic-router'
     * @example
     * function App () {
     *   const [, { match }] = useRouter()
     *
     *   return (
     *     <p>Slug: {match('/projects/:slug')?.slug ?? 'undefined'}</p>
     *   )
     * }
     */ match (route) {
            return $a1adfa6e87ad916f$var$getParams(route, state.route);
        }
    };
    const listener = ()=>{
        setState('route', $a1adfa6e87ad916f$var$getRoute());
    };
    window.addEventListener('hashchange', listener);
    $4WlUo$solidjs.onCleanup(()=>{
        window.removeEventListener('hashchange', listener);
    });
    return $4WlUo$solidjsweb.createComponent($a1adfa6e87ad916f$var$RouterContext.Provider, {
        value: [
            state,
            actions
        ],
        get children () {
            return props.children;
        }
    });
}
/**
 * Wraps {@link Route} components for rendering views.
 *
 * This is simply a renamed `Switch` component exported by Solid.
 *
 * @param {object} props Component properties
 * @param {JSX} props.children Routes to match
 * @returns {JSX} Switch component
 * @see https://www.solidjs.com/docs/latest/api#%3Cswitch%3E%2F%3Cmatch%3E
 * @example
 * import { Routes } from 'solid-basic-router'
 * @example
 * <!-- JSX -->
 * <Routes fallback={<h2>404 Page</h2>}>
 *   <Route is='/'><h2>Home Page</h2></Route>
 *   <Route is='/about'><h2>About Page</h2></Route>
 * </Routes>
 * @example
 * <!-- Manually creating a router -->
 * <Switch fallback={<h2>404 Page</h2>}>
 *   <Match when={is('/')}><h2>Home Page</h2></Match>
 *   <Match when={is('/about')}><h2>About Page</h2></Match>
 * </Switch>
 */ function $a1adfa6e87ad916f$var$Routes(props) {
    return $4WlUo$solidjsweb.createComponent($4WlUo$solidjsweb.Switch, $4WlUo$solidjsweb.mergeProps(props, {
        get children () {
            return props.children;
        }
    }));
}
/**
 * Changes the current route.
 *
 * Setting the replace parameter to `true` is useful for preventing history from using
 * routes that are no longer accessible.
 *
 * @param {string} route Route to navigate to
 * @param {boolean} [replace=false] Use location.replace instead of hashchange event
 * @example
 * import { setRoute } from 'solid-basic-router'
 * @example
 * // Using the hashchange event
 * setRoute('/projects/alpha')
 * @example
 * // Using the replace function
 * setRoute('/projects', true)
 */ function $a1adfa6e87ad916f$var$setRoute(route, replace = false) {
    if (replace) window.location.replace('#' + route);
    else window.location.hash = route;
}
/**
 * Router context accessor.
 *
 * @returns {object[]} Router state and actions objects
 * @see RouterState
 * @see RouterActions
 * @example
 * import { useRouter } from 'solid-basic-router'
 * @example
 * function App () {
 *   [routerState, routerActions] = useRouter()
 * }
 */ function $a1adfa6e87ad916f$var$useRouter() {
    return $4WlUo$solidjs.useContext($a1adfa6e87ad916f$var$RouterContext);
}


//# sourceMappingURL=index.js.map
