/* global describe expect test */
import { Link, Route, RouterSettings, Routes } from '..'
import { renders, resets, routes } from './helpers'

resets()

describe('Router', () => {
  describe('Link', () => {
    test('basic usage', () => {
      const App = () => {
        return (
          <>
            <Link to='/'>Home</Link>
            <Link to='/about'>About</Link>
          </>
        )
      }

      const { container } = renders(App)

      expect(container).toMatchSnapshot('on home page')

      routes('/about')

      expect(container).toMatchSnapshot('on about page')
    })

    test('/projects exact', () => {
      const App = () => {
        return (
          <>
            <Link to='/projects'>Fuzzy</Link>
            <Link to='/projects' exact>Exact</Link>
          </>
        )
      }

      const { container } = renders(App)

      routes('/projects')

      expect(container).toMatchSnapshot('on projects page')

      routes('/projects/alpha')

      expect(container).toMatchSnapshot('on alpha page')
    })

    test('/projects exact={false}', () => {
      RouterSettings.exactActiveLinks = true

      const App = () => {
        return (
          <>
            <Link to='/projects'>Exact</Link>
            <Link to='/projects' exact={false}>Fuzzy</Link>
          </>
        )
      }

      const { container } = renders(App)

      routes('/projects')

      expect(container).toMatchSnapshot('on projects page')

      routes('/projects/alpha')

      expect(container).toMatchSnapshot('on alpha page')
    })

    test('/about active', () => {
      const App = () => {
        return (
          <>
            <Link to='/'>Home</Link>
            <Link to='/about' active='ui.active'>About</Link>
          </>
        )
      }

      const { container } = renders(App)

      expect(container).toMatchSnapshot('on home page')

      routes('/about')

      expect(container).toMatchSnapshot('on about page')
    })

    test('/about active={false}', () => {
      RouterSettings.activeCssClass = 'ui.active'

      const App = () => {
        return (
          <>
            <Link to='/'>Home</Link>
            <Link to='/about' active={false}>About</Link>
          </>
        )
      }

      const { container } = renders(App)

      expect(container).toMatchSnapshot('on home page')

      routes('/about')

      expect(container).toMatchSnapshot('on about page')
    })
  })

  describe('Routes/Route', () => {
    test('basic usage', () => {
      const App = () => {
        return (
          <Routes>
            <Route is='/'><h2>Home Page</h2></Route>
            <Route is='/about'><h2>About Page</h2></Route>
          </Routes>
        )
      }

      const { container } = renders(App)

      expect(container).toMatchSnapshot('on home page')

      routes('/about')

      expect(container).toMatchSnapshot('on about page')
    })

    test('404', () => {
      const App = () => {
        return (
          <Routes fallback={<h2>404 Page</h2>}>
            <Route is='/'><h2>Home Page</h2></Route>
          </Routes>
        )
      }

      const { container } = renders(App)

      expect(container).toMatchSnapshot('on home page')

      routes('/asdf')

      expect(container).toMatchSnapshot('on 404 page')
    })

    test('view', () => {
      const App = () => {
        return (
          <Routes>
            <Route is='/' view={<h2>Home Page</h2>} />
            <Route is='/about' view={<h2>About Page</h2>} />
          </Routes>
        )
      }

      const { container } = renders(App)

      expect(container).toMatchSnapshot('on home page')

      routes('/about')

      expect(container).toMatchSnapshot('on about page')
    })

    test('match (avoid)', () => {
      const App = () => {
        return (
          <Routes>
            <Route match='/projects'><h2>Projects Page</h2></Route>
            <Route is='/projects/alpha'><h2>Alpha Project</h2></Route>
          </Routes>
        )
      }

      const { container } = renders(App)

      routes('/projects')

      expect(container).toMatchSnapshot('on projects page')

      routes('/projects/alpha')

      // match(/projects) returns true before is(/projects/alpha) can be matched
      expect(container).toMatchSnapshot('on projects page')
    })

    test('match (prefer)', () => {
      const App = () => {
        return (
          <Routes>
            <Route is='/projects/alpha'><h2>Alpha Project</h2></Route>
            <Route match='/projects'><h2>Projects Page</h2></Route>
          </Routes>
        )
      }

      const { container } = renders(App)

      routes('/projects')

      expect(container).toMatchSnapshot('on projects page')

      routes('/projects/alpha')

      expect(container).toMatchSnapshot('on alpha project')
    })

    test('match (nested)', () => {
      const App = () => {
        return (
          <Routes>
            <Route match='/projects'>
              <h2>Projects Page</h2>
              <Routes>
                <Route is='/projects/alpha'>
                  <h3>Alpha Project</h3>
                </Route>
              </Routes>
            </Route>
          </Routes>
        )
      }

      const { container } = renders(App)

      routes('/projects')

      expect(container).toMatchSnapshot('on projects page')

      routes('/projects/alpha')

      expect(container).toMatchSnapshot('on alpha project')
    })
  })
})
