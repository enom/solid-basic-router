/* global expect test */
import { getParams, getRoute, setRoute } from '..'
import { resets, routes } from './helpers'

resets()

test('getParams', () => {
  expect(getParams('/'))
    .toEqual({})

  expect(getParams('/projects', '/projects'))
    .toEqual({})

  expect(getParams('/projects', '/projects/alpha'))
    .toEqual({})

  expect(getParams('/projects/:slug', '/projects/alpha'))
    .toEqual({ slug: 'alpha' })

  expect(getParams('/projects/:slug/authors/:user', '/projects/alpha/authors/charlie'))
    .toEqual({ slug: 'alpha', user: 'charlie' })

  expect(getParams('/projects/:slug/authors/:user', '/authors/charlie'))
    .not.toBeDefined()
})

test('getParams trailing slash', () => {
  expect(getParams('/projects/:slug', '/projects'))
    .not.toBeDefined()

  expect(getParams('/projects/:slug', '/projects/'))
    .toEqual({ slug: '' })
})

test('getRoute', () => {
  expect(window.location.hash).toEqual('')
  expect(getRoute()).toEqual('/')

  routes('/')

  expect(window.location.hash).toEqual('#/')
  expect(getRoute()).toEqual('/')

  routes('/about')

  expect(window.location.hash).toEqual('#/about')
  expect(getRoute()).toEqual('/about')
})

test('setRoute', () => {
  expect(window.location.hash).toEqual('')

  setRoute('/about')

  expect(window.location.hash).toEqual('#/about')

  setRoute('/projects', true)

  // Attempts to mock and test window.location.replace have been unsuccessful
  expect(window.location.hash).toEqual('#/projects')
})
