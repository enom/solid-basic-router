/* global beforeEach JSX */
import { render } from 'solid-testing-library'

import { Router, RouterSettings } from '..'

/**
 * Renders a router wrapped application.
 *
 * @param {JSX} App Application to render
 * @returns {JSX} The rendered component
 */
export function renders (App) {
  return render(() => (<Router><App /></Router>))
}

/**
 * Resets the location hash and router settings
 *
 * @returns {void}
 */
export function resets () {
  RouterSettings.configure({
    activeCssClass: 'active',
    exactActiveLinks: false
  })

  beforeEach(() => {
    window.location.hash = ''
  })
}

/**
 * Changes the current hash location and fires a hashchange event.
 *
 * @param {string} hash New hash location
 * @returns {void}
 */
export function routes (hash) {
  window.location.hash = hash
  window.dispatchEvent(new HashChangeEvent('hashchange'))
}
