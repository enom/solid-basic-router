/* global expect test */
import { RouterSettings } from '..'

test('settings', () => {
  const defaults = {
    activeCssClass: 'active',
    exactActiveLinks: false
  }
  const settings = {
    activeCssClass: 'ui.active',
    exactActiveLinks: true
  }

  expect(RouterSettings).toMatchObject(defaults)

  RouterSettings.configure(settings)

  expect(RouterSettings).toMatchObject(settings)
})
