/* global describe expect test */
import { useRouter } from '..'
import { renders, resets, routes } from './helpers'

resets()

describe('useRouter', () => {
  describe('state', () => {
    test('route', () => {
      const App = () => {
        const [state] = useRouter()

        expect(state.route).toEqual('/')

        routes('/about')

        expect(state.route).toEqual('/about')
      }

      renders(App)
    })
  })

  describe('actions', () => {
    test('is', () => {
      const App = () => {
        const [, { is }] = useRouter()

        expect(is('/')).toEqual(true)
        expect(is('/about')).toEqual(false)

        routes('/about')

        expect(is('/')).toEqual(false)
        expect(is('/about')).toEqual(true)
      }

      renders(App)
    })

    test('match', () => {
      const App = () => {
        const [, { match }] = useRouter()

        expect(match('/projects')).not.toBeDefined()

        routes('/projects')

        expect(match('/projects')).toEqual({})
        expect(match('/projects/:slug')).not.toBeDefined()

        routes('/projects/alpha')

        expect(match('/projects/:slug')).toEqual({ slug: 'alpha' })
      }

      renders(App)
    })

    test('to', () => {
      const App = () => {
        const [, { to }] = useRouter()

        expect(window.location.hash).toEqual('')

        to('/about')

        expect(window.location.hash).toEqual('#/about')
      }

      renders(App)
    })
  })
})
