/* global JSX */

/**
 * Basic router actions.
 *
 * @namespace RouterActions
 */

/**
 * Basic router state.
 *
 * @namespace RouterState
 * @property {string} route Current route
 * @example
 * import { useRouter } from 'solid-basic-router'
 * @example
 * // Accessing the current route
 * function App () {
 *   [router] = useRouter()
 *
 *   return (
 *     <p>Current route: {router.route}</p>
 *   )
 * }
 */

import { createContext, onCleanup, useContext } from 'solid-js'
import { createStore } from 'solid-js/store'

const RouterContext = createContext()

/**
 * Global router settings.
 *
 * These are not meant to be reactive.
 *
 * @namespace RouterSettings
 * @property {string} [activeCssClass] CSS class to add to active all Link components
 * @property {boolean} [exactActiveLinks] Use exact route matching for all Link components
 * @see RouterSettings#configure
 * @example
 * import { RouterSettings } from 'solid-basic-router'
 * @example
 * // Manually configuring the router
 * RouterSettings.activeCssClass = 'ui.active'
 */
export const RouterSettings = {
  activeCssClass: 'active',
  exactActiveLinks: false,
  /**
   * Configures the global router settings.
   *
   * @function RouterSettings#configure
   * @param {object} settings Settings to configure
   * @returns {void}
   * @see RouterSettings
   * @example
   * import { RouterSettings } from 'solid-basic-router'
   * @example
   * // Using the `configure` helper method
   * RouterSettings.configure({
   *   activeCssClass: 'ui.active',
   *   exactActiveLinks: true
   * })
   */
  configure (settings) {
    Object.assign(RouterSettings, settings)
  }
}

/**
 * Returns the matched parameters of a given route as an object.
 *
 * Parameters are specified using ":" followed by the capture name desired(e.g. ":slug").
 *
 * @param {string} match Parameterized route
 * @param {string} route Route to match
 * @returns {object|undefined} Matched route parameters or undefined
 * @example
 * import { getParams } from 'solid-basic-router'
 * @example
 * // Even though no parameters are being captured, since the route matches, it returns an object
 * console.log(getParams('/projects', '/projects/alpha/authors/charlie'))
 * // object: {}
 * @example
 * // Specifying parameters
 * console.log(getParams('/projects/:slug', '/projects/alpha/authors/charlie'))
 * // object: { slug: 'alpha' }
 * @example
 * // Specifying multiple parameters
 * console.log(getParams('/projects/:slug/authors/:username', '/projects/alpha/authors/charlie'))
 * // object: { slug: 'alpha', username: 'charlie' }
 * @example
 * // Route doesn't match so undefined is returned
 * console.log(getParams('/projects/:slug/authors/:username', '/authors/charlie'))
 * // undefined
 */
export function getParams (match, route) {
  if (match === '/') return {}

  const params = {}
  const matched = match.slice(1).split('/')
  const routed = route.slice(1).split('/')
  const length = routed.length > matched.length ? routed.length : matched.length

  for (let i = 0; i < length; i++) {
    if (routed[i] === undefined) return
    if (matched[i] === undefined) return params

    if (matched[i][0] === ':') {
      params[matched[i].slice(1)] = routed[i]
    } else if (matched[i] !== routed[i]) {
      return
    }
  }

  return params
}

/**
 * Returns the current route based on the location hash.
 *
 * @returns {string} Current route
 * @example
 * // Location: http://localhost/
 * console.log(getRoute())
 * // string: '/'
 * @example
 * // Location: http://localhost/#/about
 * console.log(getRoute())
 * // string: '/about'
 */
export function getRoute () {
  const hash = window.location.hash
  return hash ? hash.slice(1) : '/'
}

/**
 * Creates an anchor tag to the given route.
 *
 * The contents of the link can also be specified using `props.label`.
 *
 * By default, the link will inherit the {@link RouterSettings} `activeCssClass`
 * class when the current route matches the given one. The added class can be
 * overriden per component by passing one to `props.active` or disabled by passing
 * `false`.
 *
 * The matching behaviour can be adjusted using `props.exact` or by setting the
 * {@link RouterSettings} `exactActiveLinks` to `true`. The global setting can also
 * be disabled per component by passing `false` to `props.exact`.
 *
 * Using exact matching leverages {@link RouterActions#is} while fuzzy matching
 * uses {@link RouterActions#match}. Both of these require the application code to
 * be wrapped with a {@link Router}.
 *
 * @param {object} props Component properties
 * @param {string} props.to Route to link to
 * @param {string} [props.active] Active CSS class to add
 * @param {string|JSX} [props.label] Label to display instead of component children
 * @returns {JSX} Configured anchor tag
 * @see RouterSettings
 * @see RouterActions#is
 * @see RouterActions#match
 * @example
 * import { Link } from 'solid-basic-router'
 * @example
 * <!-- JSX -->
 * <Link to='/'>Home</Link>
 * @example
 * <!-- Using the `label` property -->
 * <Link to='/' label='Home' />
 * @example
 * <!-- Using the `active` property -->
 * <Link to='/' active='ui.active'>Home</Link>
 * @example
 * <!-- Disabling the global `activeCssClass` setting -->
 * <Link to='/' active={false}>Home</Link>
 * @example
 * <!-- Using the `exact` property -->
 * <Link to='/projects' exact>Projects</Link>
 * @example
 * <!-- Disabling the global `exactActiveLinks` setting -->
 * <Link to='/projects' exact={false}>Projects</Link>
 */
export function Link (props) {
  const [, { is, match }] = useRouter()
  const resolve = (to) => {
    if (props.exact === true) return is(to)
    if (RouterSettings.exactActiveLinks && props.exact !== false) return is(to)
    return to === '/' ? is(to) : match(to)
  }
  const list = () => {
    const results = props.classList ?? {}

    if (props.active) {
      return Object.assign(results, { [props.active]: resolve(props.to) })
    }

    if (RouterSettings.activeCssClass && props.active !== false) {
      return Object.assign(results, { [RouterSettings.activeCssClass]: resolve(props.to) })
    }

    return results
  }

  return (
    <a href={'#' + props.to} classList={list()}>
      {props.label ?? props.children}
    </a>
  )
}

/**
 * Handles matching a given route and displaying its contents.
 *
 * This is simply a configured `Match` component exported by Solid. One of `props.is`
 * or `props.match` is required.
 *
 * The contents to render can also be specified using `props.view`.
 *
 * Parent must be a {@link Routes} or `Switch` component.
 *
 * @function Route
 * @param {object} props Component properties
 * @param {string} [props.is] Exact route to match
 * @param {string} [props.match] Route to match
 * @param {JSX} [props.view] View to render
 * @returns {JSX} Configured Match component
 * @see RouterActions#is
 * @see RouterActions#match
 * @see https://www.solidjs.com/docs/latest/api#%3Cswitch%3E%2F%3Cmatch%3E
 * @example
 * import { Route } from 'solid-basic-router'
 * @example
 * <!-- JSX -->
 * <Route is='/'><h2>Home Page</h2></Route>
 * @example
 * <!-- Using the `view` property -->
 * <Route is='/' view={<h2>Home Page</h2>} />
 * @example
 * <!-- Using the `match` property -->
 * <Route match='/projects'><h2>Projects <small>and Sub Projects</small> Pages</h2></Route>
 * @example
 * <!-- Manually creating a route -->
 * <Match when={is('/')}><h2>Home Page</h2></Match>
 */
export function Route (props) {
  const [, { is, match }] = useRouter()

  return (
    <Match when={props.match ? match(props.match) : is(props.is)}>
      {props.view ?? props.children}
    </Match>
  )
}

/**
 * Router context provider.
 *
 * @param {object} props Router properties
 * @param {JSX} props.children JSX to make router aware
 * @returns {JSX} Router aware JSX
 * @example
 * import { render } from 'solid-js/web'
 * import { Router } from 'solid-basic-router'
 * import App from './App'
 *
 * render(() => (
 *   <Router>
 *     <App />
 *   </Router>
 * ), document.getElementById('root'))
 */
export function Router (props) {
  const [state, setState] = createStore({ route: getRoute() })

  const actions = {
    /**
     * Changes the current route to another.
     *
     * @function RouterActions#to
     * @param {string} route Route to navigate to
     * @param {boolean} [replace=false] Use location.replace instead of hashchange event
     * @returns {void}
     * @see setRoute
     * @example
     * import { useRouter } from 'solid-basic-router'
     * @example
     * function App () {
     *   const [, { to }] = useRouter()
     *
     *   return (
     *     <button onClick={() => to('/')}>To Home Page</button>
     *   )
     * }
     */
    to: setRoute,
    /**
     * Checks if the current route matches the given one exactly.
     *
     * Trailing slashes matter:
     *
     * - `/about` does not match `/about/` and vice versa
     *
     * @function RouterActions#is
     * @param {string} route Route to check
     * @returns {boolean} Route matches exactly
     * @example
     * import { useRouter } from 'solid-basic-router'
     * @example
     * function App () {
     *   const [, { is }] = useRouter()
     *
     *   return (
     *     <p>Are we currently on the home page? {is('/') ? 'Yes' : 'No'}</p>
     *   )
     * }
     */
    is (route) {
      return route === state.route
    },
    /**
     * Matches the current route to a given one and returns the captured parameters.
     *
     * Trailing slashes matter:
     *
     * - `/projects` matches `/projects`, `/projects/alpha`, and `projects/bravo`
     * - `/projects/` matches `projects/alpha` and `projects/bravo` but not `/projects`
     *
     * @function RouterActions#match
     * @param {string} route Route to check
     * @returns {object|undefined} Matched route parameters or undefined
     * @see getParams
     * @example
     * import { useRouter } from 'solid-basic-router'
     * @example
     * function App () {
     *   const [, { match }] = useRouter()
     *
     *   return (
     *     <p>Slug: {match('/projects/:slug')?.slug ?? 'undefined'}</p>
     *   )
     * }
     */
    match (route) {
      return getParams(route, state.route)
    }
  }

  const listener = () => {
    setState('route', getRoute())
  }

  window.addEventListener('hashchange', listener)

  onCleanup(() => {
    window.removeEventListener('hashchange', listener)
  })

  return (
    <RouterContext.Provider value={[state, actions]}>
      {props.children}
    </RouterContext.Provider>
  )
}

/**
 * Wraps {@link Route} components for rendering views.
 *
 * This is simply a renamed `Switch` component exported by Solid.
 *
 * @param {object} props Component properties
 * @param {JSX} props.children Routes to match
 * @returns {JSX} Switch component
 * @see https://www.solidjs.com/docs/latest/api#%3Cswitch%3E%2F%3Cmatch%3E
 * @example
 * import { Routes } from 'solid-basic-router'
 * @example
 * <!-- JSX -->
 * <Routes fallback={<h2>404 Page</h2>}>
 *   <Route is='/'><h2>Home Page</h2></Route>
 *   <Route is='/about'><h2>About Page</h2></Route>
 * </Routes>
 * @example
 * <!-- Manually creating a router -->
 * <Switch fallback={<h2>404 Page</h2>}>
 *   <Match when={is('/')}><h2>Home Page</h2></Match>
 *   <Match when={is('/about')}><h2>About Page</h2></Match>
 * </Switch>
 */
export function Routes (props) {
  return (
    <Switch {...props}>
      {props.children}
    </Switch>
  )
}

/**
 * Changes the current route.
 *
 * Setting the replace parameter to `true` is useful for preventing history from using
 * routes that are no longer accessible.
 *
 * @param {string} route Route to navigate to
 * @param {boolean} [replace=false] Use location.replace instead of hashchange event
 * @example
 * import { setRoute } from 'solid-basic-router'
 * @example
 * // Using the hashchange event
 * setRoute('/projects/alpha')
 * @example
 * // Using the replace function
 * setRoute('/projects', true)
 */
export function setRoute (route, replace = false) {
  if (replace) {
    window.location.replace('#' + route)
  } else {
    window.location.hash = route
  }
}

/**
 * Router context accessor.
 *
 * @returns {object[]} Router state and actions objects
 * @see RouterState
 * @see RouterActions
 * @example
 * import { useRouter } from 'solid-basic-router'
 * @example
 * function App () {
 *   [routerState, routerActions] = useRouter()
 * }
 */
export function useRouter () {
  return useContext(RouterContext)
}
